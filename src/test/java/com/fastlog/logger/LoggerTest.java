package com.fastlog.logger;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author Andrew Osipenko
 */
public class LoggerTest {
    @Test
    public void testNoExceptionsWithoutLogConsumers() throws InterruptedException {
        Logger.log(1, "test");
    }

    @Test
    public void testFileLogConsumer() throws InterruptedException, IOException {
        File file = File.createTempFile("logger-test", ".log");
        FileLogConsumer fileLogConsumer = new FileLogConsumer(file);
        try {
            Logger.registerLogConsumer(fileLogConsumer);
            Logger.log(1, "test1");
            Logger.log(2, "test2");
            Logger.unregisterLogConsumer(fileLogConsumer);
        }
        finally {
            fileLogConsumer.close();
        }
        assertFileText(file, "[1] test1\n[2] test2\n");
        file.delete();
    }

    private void assertFileText(File file, String text) throws IOException {
        FileReader fileReader = new FileReader(file);
        try{
            StringBuilder sb = new StringBuilder();
            int c;
            while((c = fileReader.read()) != -1){
                sb.append((char)c);
            }
            Assert.assertEquals(text, sb.toString());
        }
        finally {
            fileReader.close();
        }
    }
}
