package com.fastlog.executor;

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import static org.junit.Assert.assertEquals;

/**
 * @author Andrew Osipenko
 */
public class SingleThreadQueueJobExecutorTest {
    private volatile JobExecutor jobExecutor;
    private volatile AtomicInteger counter;
    private volatile Runnable counterIncrementer = new Runnable() {
        public void run() {
            counter.incrementAndGet();
        }
    };

    @Before
    public void setup(){
        jobExecutor = new SingleThreadQueueJobExecutor();
        counter = new AtomicInteger();
    }

    @Test(expected = IllegalStateException.class)
    public void testShutdown() throws InterruptedException {
        jobExecutor.shutdown();
        jobExecutor.execute(counterIncrementer);
    }
    /**
     * Test if the job can be run by the thread who created the {@link SingleThreadQueueJobExecutor}
     * @throws InterruptedException
     */
    @Test
    public void testCreatorThreadJob() throws InterruptedException {
        jobExecutor.execute(counterIncrementer);
        jobExecutor.execute(counterIncrementer);
        jobExecutor.execute(counterIncrementer);
        jobExecutor.execute(counterIncrementer);
        jobExecutor.execute(counterIncrementer);
        waitJobExecutor();
        assertEquals(5, counter.get());
    }
    /**
     * Test if the job can be run by other threads.
     * @throws InterruptedException
     */
    @Test
    public void testOtherThreadJobs() throws InterruptedException {
        List<Thread> threads = new ArrayList<Thread>();
        for(int i = 0; i < 10; i++){
            Thread thread = new Thread(new Runnable() {
                public void run() {
                    for(int j = 0; j < 10; j++) {
                        jobExecutor.execute(counterIncrementer);
                    }
                }
            });
            thread.start();
            threads.add(thread);
        }
        for(Thread thread: threads){
            thread.join();
        }
        waitJobExecutor();
        assertEquals(100, counter.get());
    }

    @Test
    public void testJobExecutionOrder(){
        // TODO: implement
    }

    private void waitJobExecutor() throws InterruptedException {
        jobExecutor.shutdown();
    }
}
