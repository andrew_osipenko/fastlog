package com.fastlog.executor;

/**
 * Executes jobs.
 *
 * @author Andrew Osipenko
 */
public interface JobExecutor {
    /**
     * Executes the <code>job</code>. Implementors can execute the job asynchronously.
     *
     * @param job required, the job to be executed.
     * @throws IllegalStateException in case the executor have been shutdown already.
     */
    void execute(Runnable job) throws IllegalStateException;

    /**
     * Shutdown the job executor.
     */
    void shutdown();
}
