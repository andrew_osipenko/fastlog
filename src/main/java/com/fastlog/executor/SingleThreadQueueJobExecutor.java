package com.fastlog.executor;

import java.util.ArrayList;
import java.util.List;

/**
 * Executes jobs in a separate thread in the order the jobs are passed. Uses a queue internally to
 * handle job execution.
 *
 * The class is thread safe.
 *
 * @author Andrew Osipenko
 */
public class SingleThreadQueueJobExecutor implements JobExecutor {
    private final Thread thread;
    private final List<Runnable> jobQueue;
    private boolean shutdown;

    public SingleThreadQueueJobExecutor() {
        thread = new Thread(new JobRunnable());
        jobQueue = new ArrayList<Runnable>();
        thread.start();
    }

    /**
     * Execute runnable in a separate thread.
     *
     * @param runnable required
     * @throws IllegalStateException
     */
    public void execute(Runnable runnable) throws IllegalStateException {
        synchronized (jobQueue){
            if(shutdown){
                throw new IllegalStateException("The executor is shutted down already");
            }
            jobQueue.add(runnable);
            if(jobQueue.size() == 1) {
                jobQueue.notify();
            }
        }
    }

    /**
     * Requests a shutdown and waits until the jobs which are in the queue are finished.
     * No new jobs will be added to the queue.
     */
    public void shutdown() {
        synchronized (jobQueue) {
            if (shutdown) {
                return;
            }
            shutdown = true;
            thread.interrupt();
        }
        try {
            thread.join();
        } catch (InterruptedException e) {
            throw new RuntimeException("Failed to wait job finishing", e);
        }
    }

    private class JobRunnable implements Runnable {
        public void run() {
            while(!shutdown){
                Runnable runnable = null;
                synchronized (jobQueue){
                    try {
                        if(jobQueue.isEmpty()) {
                            jobQueue.wait();
                        }
                    } catch (InterruptedException e) {
                        // interrupted by shutdown
                        continue;
                    }
                    runnable = jobQueue.remove(0);
                }
                runnable.run();
            }
            synchronized (jobQueue){
                while (!jobQueue.isEmpty()){
                    jobQueue.remove(0).run();
                }
            }
        }
    }
}
