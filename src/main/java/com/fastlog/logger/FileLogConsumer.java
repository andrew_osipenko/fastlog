package com.fastlog.logger;

import java.io.*;

/**
 * Prints logs to a file.
 * Not thread safe.
 *
 * @author Andrew Osipenko
 */
public class FileLogConsumer implements LogConsumer {
    private Writer writer;
    public FileLogConsumer(File file) throws IOException {
        writer = new BufferedWriter(new FileWriter(file));
    }
    public void log(int severity, String message) throws IOException {
        if(writer == null){
            throw new IllegalStateException("Closed already");
        }
        writer.write('[');
        writer.write(Integer.toString(severity));
        writer.write("] ");
        writer.write(message);
        writer.write('\n');
    }

    public void close() throws IOException {
        writer.close();
        writer = null;
    }
}
