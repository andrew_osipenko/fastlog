package com.fastlog.logger;

import com.fastlog.executor.JobExecutor;
import com.fastlog.executor.SingleThreadQueueJobExecutor;

import java.io.IOException;
import java.util.*;

/**
 * Facade logging class with convenient static methods.
 * Allows to register {@link LogConsumer}s.
 * Under the hood a separate {@link Thread} is created to log messages into each {@link LogConsumer}
 *
 * @author Andrew Osipenko
 */
public class Logger {
    private static final Logger logger = new Logger();
    private final Map<LogConsumer, JobExecutor> logConsumers;
    private volatile int severity;

    private Logger(){
        logConsumers = new HashMap<LogConsumer, JobExecutor>();
    }

    /**
     *
     * @param logConsumer required
     * @throws IllegalArgumentException in case the logConsumer is registered already
     */
    public static void registerLogConsumer(LogConsumer logConsumer) throws IllegalArgumentException {
        synchronized (logger.logConsumers) {
            if(logger.logConsumers.containsKey(logConsumer)){
                throw new IllegalArgumentException("LogConsumer " + logConsumer + "is registered already");
            }
            logger.logConsumers.put(logConsumer, new SingleThreadQueueJobExecutor());
        }
    }

    /**
     * Unregister logConsumer from the Logger.
     * The method waits until all pending log entries are logged into log consumer.
     *
     * @param logConsumer
     */
    public static void unregisterLogConsumer(LogConsumer logConsumer){
        synchronized (logger.logConsumers) {
            JobExecutor jobExecutor = logger.logConsumers.remove(logConsumer);
            if(jobExecutor != null){
                jobExecutor.shutdown();
            }
        }
    }

    public void setSeverity(int severity){
        this.severity = severity;
    }
    public static void log(int severity, String message){
        if(severity < logger.severity){
            return;
        }
        synchronized (logger.logConsumers) {
            for(Map.Entry<LogConsumer, JobExecutor> entry: logger.logConsumers.entrySet()){
                LogConsumer logConsumer = entry.getKey();
                JobExecutor jobExecutor = entry.getValue();
                jobExecutor.execute(new LogConsumerRunnable(logConsumer, severity, message));
            }
        }
    }
    private static class LogConsumerRunnable implements Runnable {
        private LogConsumer logConsumer;
        private int severity;
        private String message;

        private LogConsumerRunnable(LogConsumer logConsumer, int severity, String message){
            this.logConsumer = logConsumer;
            this.severity = severity;
            this.message = message;
        }
        public void run() {
            try {
                logConsumer.log(severity, message);
            } catch (IOException e) {
                // catch. Do not kill the thread.
                e.printStackTrace();
            }
        }
    }
}
