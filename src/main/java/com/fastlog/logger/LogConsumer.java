package com.fastlog.logger;

import java.io.IOException;

/**
 * Consumes logs provided by {@link Logger}.
 * Not thread safe.
 *
 * @author Andrew Osipenko
 */
public interface LogConsumer {
    /**
     * Logs <code>message</code> to some endpoint (file, network, DB and so on).
     *
     * @param severity
     * @param message required
     * @throws IllegalStateException in case the method is called after closing.
     */
    void log(int severity, String message) throws IllegalStateException, IOException;

    /**
     * Flush all logs and close all open resources.
     */
    void close() throws IOException;
}
